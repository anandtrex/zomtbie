# Zomtbie

[Zomtbie](https://gitlab.com/anandtrex/zomtbie) is an open source app for [Zotero](https://www.zotero.org/) that is
built with [flutter](https://flutter.dev/) and works on both Android and iOS.

## Development setup

1. Install the [flutter development environment](https://flutter.dev/docs/get-started/install).
1. Create a [Zotero](https://www.zotero.org/) account and register an application in your [account settings](https://www.zotero.org/oauth/apps).
1. Create the file `lib/env/production_secrets.dart` with the contents:
    ```
    import '../models/secrets.dart';

    class ProductionSecrets implements PreOAuthSecrets {
      @override String get zoteroApiKey => 'asdf123ihg';
      @override String get zoteroApiSecret => '321fdsaghi';
    }
    ```
    where you replace the API Key and Secret with the ones Zotero provides above.
1. Build and test the app.

Optional: Read the [Zotero API documentation](https://www.zotero.org/support/dev/web_api/v3/start).

Icon design by [Print me Some Color](http://printmesomecolor.com/).
