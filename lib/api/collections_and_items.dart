import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:oauth1/oauth1.dart' as oauth1;
import 'package:flutter_cache_store/flutter_cache_store.dart';
import 'package:zomtbie/models/group.dart';

import '../consts.dart';
import '../models/collection.dart';
import '../models/item.dart';
import '../models/shared_prefs.dart';
import '../storage/shared_prefs.dart';

// A function that converts a response body into a List<Photo>.
List<Collection> _parseCollections(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Collection>((json) => Collection.fromJson(json)).toList();
}

Future<List<Collection>> fetchCollections(
    Collection collection, Group group, int pageKey, int pageSize,
    [String searchQuery = ""]) async {
  final UserDetails userDetails = await getUserDetailsToSharedPrefs();
  final oauth1.Credentials tokenCredentials =
      await getTokenCredentialsFromSharedPrefs(FINAL_TOKEN_CREDENTIALS_PREFIX);

  if (userDetails != null && tokenCredentials != null) {
    final String zoteroApiKey = tokenCredentials?.tokenSecret;

    final queryParameters = {
      'start': '$pageKey',
      'limit': '$pageSize',
      'sort': 'title'
    };
    print("searchQuery: $searchQuery");
    if (searchQuery.length > 0) {
      queryParameters
          .addAll({'q': '$searchQuery', 'itemType': '-attachment || note'});
    }
    final userOrGroupPrefix =
        group == null ? '/users/${userDetails.userID}' : '/groups/${group.id}';
    final uri = collection?.key == null
        ? Uri.https(ZOTERO_BASE_URL, '$userOrGroupPrefix/collections/top',
            queryParameters)
        : Uri.https(
            ZOTERO_BASE_URL,
            '$userOrGroupPrefix/collections/${collection.key}/collections',
            queryParameters);
    print("URI is: $uri");

    // final String response_body = await makeRequest(uri, zoteroApiKey);

    final store = await CacheStore.getInstance();
    final file = await store.getFile(uri.toString(),
        headers: {'Zotero-API-Key': zoteroApiKey, 'Zotero-API-Version': '3'});
    final String response_body = await file.readAsString();

    return compute(_parseCollections, response_body);
  }
}

Future<String> makeRequest(uri, zoteroApiKey) async {
  try {
    final response = await http.get(uri,
        headers: {'Zotero-API-Key': zoteroApiKey, 'Zotero-API-Version': '3'});
    if (response.statusCode == 200) {
      return response.body;
      // return compute(_parseCollections, response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load collections');
    }
  } catch (err) {
    print("Error occurred: $err");
  }
}

// A function that converts a response body into a List<Photo>.
List<Item> _parseItems(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Item>((json) => Item.fromJson(json)).toList();
}

Future<List<Item>> fetchItems(
    Collection collection, Group group, int pageKey, int pageSize,
    [String searchQuery = ""]) async {
  final UserDetails userDetails = await getUserDetailsToSharedPrefs();
  final oauth1.Credentials tokenCredentials =
      await getTokenCredentialsFromSharedPrefs(FINAL_TOKEN_CREDENTIALS_PREFIX);

  if (userDetails != null && tokenCredentials != null) {
    final String zoteroApiKey = tokenCredentials?.tokenSecret;

    var queryParameters = {
      'start': '$pageKey',
      'limit': '$pageSize',
      'sort': 'title'
    };
    if (searchQuery.length > 0) {
      queryParameters
          .addAll({'q': '$searchQuery', 'itemType': '-attachment || note'});
    }
    final userOrGroupPrefix =
        group == null ? '/users/${userDetails.userID}' : '/groups/${group.id}';
    final uri = collection?.key == null
        ? Uri.https(
            ZOTERO_BASE_URL, '$userOrGroupPrefix/items/top', queryParameters)
        : Uri.https(
            ZOTERO_BASE_URL,
            '$userOrGroupPrefix/collections/${collection.key}/items/top',
            queryParameters);

    // final String response_body = await makeRequest(uri, zoteroApiKey);

    final store = await CacheStore.getInstance();
    final file = await store.getFile(uri.toString(),
        headers: {'Zotero-API-Key': zoteroApiKey, 'Zotero-API-Version': '3'});
    final String response_body = await file.readAsString();

    return compute(_parseItems, response_body);
  }
}

Future<List<Item>> fetchChildItems(Item item, Group group) async {
  final UserDetails userDetails = await getUserDetailsToSharedPrefs();
  final oauth1.Credentials tokenCredentials =
      await getTokenCredentialsFromSharedPrefs(FINAL_TOKEN_CREDENTIALS_PREFIX);

  if (userDetails != null && tokenCredentials != null) {
    final String zoteroApiKey = tokenCredentials?.tokenSecret;
    final userOrGroupPrefix =
        group == null ? '/users/${userDetails.userID}' : '/groups/${group.id}';
    final uri = Uri.https(
        ZOTERO_BASE_URL, '$userOrGroupPrefix/items/${item.key}/children');
    final response = await http.get(uri,
        headers: {'Zotero-API-Key': zoteroApiKey, 'Zotero-API-Version': '3'});

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return compute(_parseItems, response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load collections');
    }
  }
}
