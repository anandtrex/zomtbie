import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:oauth1/oauth1.dart' as oauth1;

import '../consts.dart';
import '../models/group.dart';
import '../models/shared_prefs.dart';
import '../storage/shared_prefs.dart';

FutureOr<List<Group>> fetchGroups() async {
  final UserDetails userDetails = await getUserDetailsToSharedPrefs();
  final oauth1.Credentials tokenCredentials =
      await getTokenCredentialsFromSharedPrefs(FINAL_TOKEN_CREDENTIALS_PREFIX);

  if (userDetails != null && tokenCredentials != null) {
    final String zoteroApiKey = tokenCredentials?.tokenSecret;

    final uri =
        Uri.https(ZOTERO_BASE_URL, '/users/${userDetails.userID}/groups');
    // print("Groups URI is: $uri");

    try {
      final response = await http.get(uri,
          headers: {'Zotero-API-Key': zoteroApiKey, 'Zotero-API-Version': '3'});
      if (response.statusCode == 200) {
        return compute(_parseGroups, response.body);
        // return compute(_parseCollections, response.body);
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load collections');
      }
    } catch (err) {
      print("Error occurred: $err");
    }
  }
}

List<Group> _parseGroups(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Group>((json) => Group.fromJson(json)).toList();
}
