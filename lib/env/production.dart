  import './production_secrets.dart';
import '../main.dart';

void main() async {
  await startApp(ProductionSecrets());
}