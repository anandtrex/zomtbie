import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:provider/provider.dart';
import 'package:zomtbie/screens/about.dart';

import 'api/groups.dart';
import 'models/group.dart';
import 'models/provider/current_collection.dart';
import 'models/provider/current_group.dart';
import 'models/secrets.dart';
import 'screens/start.dart';

void startApp(PreOAuthSecrets secrets) async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(
      debug: false // optional: set false to disable printing logs to console
      );
  runApp(ZomtbieApp(secrets: secrets));
}

class ZomtbieApp extends StatelessWidget {
  ZomtbieApp({Key key, this.secrets}) : super(key: key);

  final PreOAuthSecrets secrets;

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider<CurrentCollection>(create: (context) => CurrentCollection()),
          Provider<CurrentGroup>(create: (context) => CurrentGroup()),
          FutureProvider<List<Group>>(
            create: (context) => fetchGroups(),
            initialData: [],
          )
        ],
        child: MaterialApp(
          title: 'Zomtbie for Zotero',
          initialRoute: '/',
          routes: {
            '/': (context) => StartScreen(secrets: secrets),
            '/about': (context) => AboutScreen(),
          },
        ));
  }
}
