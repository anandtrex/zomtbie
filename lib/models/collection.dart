class Collection {
  final String key;
  final String links_self_href;
  final String data_name;

  Collection({this.key, this.links_self_href, this.data_name});

  factory Collection.fromJson(Map<String, dynamic> json) {
    // print('NAME:');
    // print(json['data']['name']);
    return Collection(
      key: json['key'],
      links_self_href: json['links']['self']['href'],
      data_name: json['data']['name'],
    );
  }

  @override
  String toString() {
    return 'key=$key; data_name=$data_name; links_self_href=$links_self_href';
  }
}