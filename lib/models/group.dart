class Group {
  final int id;
  final String links_self_href;
  final String data_name;

  Group({this.id, this.links_self_href, this.data_name});

  factory Group.fromJson(Map<String, dynamic> json) {
    // print('NAME:');
    // print(json['data']['name']);
    return Group(
      id: json['id'],
      links_self_href: json['links']['self']['href'],
      data_name: json['data']['name'],
    );
  }

  @override
  String toString() {
    return 'Group{id=$id; data_name=$data_name; links_self_href=$links_self_href}';
  }
}