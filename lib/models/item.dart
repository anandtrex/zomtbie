import 'package:tuple/tuple.dart';

class Item {
  final String key;
  final String links_self_href;
  final String links_enclosure_href;
  final String data_title;
  final ItemCreators itemCreators;
  final ItemTags itemTags;
  final Map<String, dynamic> data;

  Item(
      {this.key,
      this.data_title,
      this.links_self_href,
      this.links_enclosure_href,
      this.data,
      this.itemCreators,
      this.itemTags});

  factory Item.fromJson(Map<String, dynamic> json) {
    return Item(
      key: json['key'],
      data_title: json['data']['title'] ?? "Untitled",
      links_self_href: json['links']['self']['href'],
      links_enclosure_href: json['links']['enclosure'] != null
          ? json['links']['enclosure']['href']
          : null,
      data: json['data'],
      itemCreators: ItemCreators.fromJson(json['data']['creators'] ?? []),
      itemTags: ItemTags.fromJson(json['data']['tags'] ?? []),
    );
  }
}

class ItemCreators {
  final List<Tuple2<String, String>> creators;

  ItemCreators({this.creators});

  factory ItemCreators.fromJson(List<dynamic> json) {
    final creators = json.map((e) {
      final creatorType = "${e['creatorType']}";
      final name = "${e['lastName']}, ${e['firstName']}";
      return Tuple2(creatorType, name);
    }).toList();
    return ItemCreators(creators: creators);
  }
}

class ItemTags {
  final List<Tuple2<String, int>> tags;

  ItemTags({this.tags});

  factory ItemTags.fromJson(List<dynamic> json) {
    final tags = json.map((e) {
      final tag = "${e['tag']}";
      final type = (e['type'] as int);
      return Tuple2(tag, type);
    }).toList();
    return ItemTags(tags: tags);
  }
}
