import 'package:stack/stack.dart';

import '../collection.dart';

class CurrentCollection {
  Stack<Collection> _collection;

  void push(Collection collection) {
    if (_collection == null) {
      _collection = Stack();
    }
    _collection.push(collection);
  }

  void pop() {
    _collection?.pop();
  }

  void clear() {
    _collection = null;
  }

  Collection get collection {
    if (_collection != null && _collection.isNotEmpty) {
      return _collection.top();
    }
  }

  @override
  String toString() {
    return 'collection=${collection.toString()}';
  }
}
