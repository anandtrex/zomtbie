import '../group.dart';

class CurrentGroup {
  Group _group;

  void update(Group group) {
    _group = group;
    print("Updated group to $group");
  }

  Group get group {
    return _group;
  }
}
