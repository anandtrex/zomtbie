abstract class PreOAuthSecrets {
  String get zoteroApiKey;
  String get zoteroApiSecret;
}