class OAuthState {
  final String token;
  final String verifier;

  OAuthState({this.token, this.verifier});

  @override
  String toString() {
    return 'oauth_token=$token&oauth_token_verifier=$verifier';
  }
}

class UserDetails {
  final String userID;
  final String username;

  UserDetails({this.userID, this.username});

  @override
  String toString() {
    return 'userID=$userID&username=$username';
  }
}