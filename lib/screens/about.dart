import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

import '../screens/appbar.dart';

class AboutScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:
            ZomtbieAppBar.getAppBar(context, 'Zomtbie', showSearchIcon: false),
        body: Column(
          children: [
            Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 50),
                  child: Image.asset(
                    "assets/papers.jpg",
                    fit: BoxFit.fitWidth,
                  ),
                ),
                Image.asset(
                  "zomtbie-circle-icon.png",
                  width: 200,
                  height: 200,
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: Text(
                'About \n Zomtbie \n for Zotero ',
                textAlign: TextAlign.center,
                style: GoogleFonts.raleway(
                  textStyle: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    backgroundColor: Color.fromARGB(100, 255, 255, 255),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  style: GoogleFonts.raleway(
                    textStyle: TextStyle(
                      fontSize: 16,
                      height: 2,
                      // fontWeight: FontWeight.bold,
                      color: Colors.black,
                      backgroundColor: Color.fromARGB(100, 255, 255, 255),
                    ),
                  ),
                  children: [
                    TextSpan(
                      text: 'Zomtbie is an open source client for Zotero\n',
                      style: TextStyle(color: Colors.black),
                    ),
                    TextSpan(
                      text: 'The source is available on ',
                      style: TextStyle(color: Colors.black),
                    ),
                    TextSpan(
                      text: 'Gitlab\n',
                      style: TextStyle(color: Colors.blue),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          _launchURL('https://gitlab.com/anandtrex/zomtbie');
                        },
                    ),
                    TextSpan(
                      text: 'This app was developed by: ',
                      style: TextStyle(color: Colors.black),
                    ),
                    TextSpan(
                      text: '@anandtrex\n',
                      style: TextStyle(color: Colors.blue),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          _launchURL('https://anand.ink/about/');
                        },
                    ),
                    TextSpan(
                      text: 'Icon design by: ',
                      style: TextStyle(color: Colors.black),
                    ),
                    TextSpan(
                      text: '@printmesomecolor\n',
                      style: TextStyle(color: Colors.blue),
                      recognizer: TapGestureRecognizer()
                        ..onTap = () {
                          _launchURL('https://www.printmesomecolor.com/links/');
                        },
                    ),
                  ],
                ),
              ),

              // Text(
              //   'Zomtbie is an open source client for Zotero \n'
              //   'Developed by Anand\n'
              //   'Icon by Ashwini',
              //   textAlign: TextAlign.center,
              //   style: GoogleFonts.raleway(
              //     textStyle: TextStyle(
              //       fontSize: 12,
              //       fontWeight: FontWeight.bold,
              //       color: Colors.black,
              //       backgroundColor: Color.fromARGB(100, 255, 255, 255),
              //     ),
              //   ),
              // ),
            ),
          ],
        ));
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
