import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../consts.dart';
import '../screens/search.dart';

class ZomtbieAppBar {
  static Widget getAppBar(
    BuildContext context,
    String title, {
    String subtitle,
    bool showSearchIcon = true,
    bool emptyLeading = false,
    TabBar tabbar,
  }) {
    final actions = <Widget>[
      IconButton(
        icon: const Icon(Icons.search, color: Colors.black),
        tooltip: 'Add new entry',
        onPressed: () {
          showSearch(context: context, delegate: Search());
        },
      ),
    ];

    return AppBar(
      automaticallyImplyLeading: true,
      leading: emptyLeading ? Container() : null,
      iconTheme: IconTheme.of(context).copyWith(color: Colors.black),
      backgroundColor: THEME_COLOR,
      title: subtitle != null
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title,
                    style: TextStyle(color: Colors.black, fontSize: 16.0)),
                Text(subtitle,
                    style: TextStyle(color: Colors.black, fontSize: 14.0)),
              ],
            )
          : Text(title, style: TextStyle(color: Colors.black, fontSize: 16.0)),
      // title: Text(title, style: TextStyle(color: Colors.black)),
      actions: showSearchIcon ? actions : [],
      bottom: tabbar,
    );
  }
}
