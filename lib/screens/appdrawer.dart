import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:zomtbie/storage/shared_prefs.dart';

import '../consts.dart';
import '../models/group.dart';
import '../models/provider/current_collection.dart';
import '../models/provider/current_group.dart';
import 'collection_item.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final currentGroup = context.watch<CurrentGroup>();
    final currentCollection = context.watch<CurrentCollection>();
    final groups = context.watch<List<Group>>();
    final header = DrawerHeader(
      child: Container(
        child: Column(
          children: [
            Text(' '),
            Text(' '),
            Image.asset(
              "zomtbie-circle-icon.png",
              width: 80,
              height: 80,
            ),
            ClipRRect(
              // borderRadius: BorderRadius.circular(20.0),
              child: Text(
                ' Zomtbie ',
                style: GoogleFonts.raleway(
                  textStyle: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    backgroundColor: Color.fromARGB(100, 255, 255, 255),
                  ),
                ),
              ),
            )
          ],
        ),
        alignment: Alignment.bottomLeft,
      ),
      decoration: BoxDecoration(
          color: THEME_COLOR,
          image: DecorationImage(
              image: AssetImage("assets/papers.jpg"), fit: BoxFit.cover)),
    );
    final myLibrary = ListTile(
      leading: Icon(
        Icons.photo_album,
        color: Colors.black,
      ),
      title: Text(
        'My Library',
        style: GoogleFonts.robotoSlab(),
      ),
      onTap: () {
        currentGroup.update(null);
        Navigator.popUntil(context, ModalRoute.withName('/'));
        // Navigator.pushNamed(context, '/');
      },
    );
    final groupLibraries = ListTile(
      dense: true,
      title: Text(
        'Group Libraries',
        style: GoogleFonts.robotoSlab(color: Colors.grey),
      ),
    );
    final logoutButton = RaisedButton(
      onPressed: () {
        clearPrefs();
        Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
      },
      child: Text("Logout", style: GoogleFonts.robotoSlab(color: Colors.black)),
    );
    final aboutButton = OutlineButton(
      onPressed: () {
        Navigator.pushNamed(context, '/about');
      },
      child: Text("About", style: GoogleFonts.robotoSlab(color: Colors.black)),
    );
    return Drawer(
      child: ListView(
        children: [
          header,
          myLibrary,
          Divider(),
          groups.length > 0? groupLibraries : Container(),
          for (var group in groups) ListTile(
            leading: Icon(
              Icons.library_books,
              color: Colors.black,
            ),
            title: Text(
              '${group.data_name}',
              style: GoogleFonts.robotoSlab(),
            ),
            onTap: () {
              currentGroup.update(group);
              currentCollection.clear();
              // Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WillPopScope(
                    onWillPop: () async {
                      currentGroup.update(null);
                      return true;
                    },
                    child: CollectionItemScreen(
                      currentGroup,
                      currentCollection,
                      key: Key(
                          '${currentGroup.group?.data_name}${currentCollection.collection?.data_name}'),
                    ),
                  ),
                ),
              );
            },
          ),
          Container(),
          logoutButton,
          aboutButton
        ]
      )
    );
  }
}
