import 'package:flutter/material.dart';

import '../models/provider/current_collection.dart';
import '../models/provider/current_group.dart';
import 'appbar.dart';
import 'appdrawer.dart';
import 'collections.dart';
import 'items.dart';

class CollectionItemScreenNoScaffold extends StatelessWidget {
  final String searchQuery;
  final CurrentGroup currentGroup;
  final CurrentCollection currentCollection;

  CollectionItemScreenNoScaffold(this.currentGroup, this.currentCollection,
      {Key key, this.searchQuery = ""})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          title: Text(
            "Collections",
            // style: GoogleFonts.robotoSlab(fontSize: 15, fontWeight: FontWeight.bold),
            style: TextStyle(fontSize: 15),
            textAlign: TextAlign.center,
          ),
          toolbarHeight: 30,
          expandedHeight: 30,
          pinned: true,
          floating: false,
          backgroundColor: Colors.black,
          leading: Container(),
        ),
        CollectionsScreen(
          key: Key("Collections" +
              (currentCollection.collection != null
                  ? currentCollection.collection.key
                  : "root")),
          currentCollection: currentCollection,
          searchQuery: searchQuery,
          currentGroup: currentGroup,
        ),
        SliverAppBar(
          title: Text(
            "Items",
            style: TextStyle(
              fontSize: 15,
            ),
            textAlign: TextAlign.center,
          ),
          toolbarHeight: 30,
          expandedHeight: 30,
          pinned: true,
          backgroundColor: Colors.black,
          leading: Container(),
        ),
        ItemsScreen(
          key: Key("Items" +
              (currentCollection.collection != null
                  ? currentCollection.collection.key
                  : "root")),
          currentCollection: currentCollection,
          searchQuery: searchQuery,
          currentGroup: currentGroup,
        ),
      ],
    );
  }
}

class CollectionItemScreen extends StatelessWidget {
  final String searchQuery;
  final CurrentGroup currentGroup;
  final CurrentCollection currentCollection;

  CollectionItemScreen(this.currentGroup, this.currentCollection,
      {Key key, this.searchQuery = ""})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appBar = ZomtbieAppBar.getAppBar(
      context,
      currentGroup.group?.data_name != null
          ? 'Library: ${currentGroup.group?.data_name}'
          : 'My Library',
      subtitle: currentCollection.collection?.data_name,
      showSearchIcon: true,
    );
    if (currentCollection.collection != null) {
      return Scaffold(
        appBar: appBar,
        body: CollectionItemScreenNoScaffold(
          currentGroup,
          currentCollection,
          searchQuery: searchQuery,
          key: Key(key.toString() + 'ns'),
        ),
      );
    } else {
      return Scaffold(
        appBar: appBar,
        drawer: AppDrawer(),
        body: CollectionItemScreenNoScaffold(
          currentGroup,
          currentCollection,
          searchQuery: searchQuery,
          key: Key(key.toString() + 'ns'),
        ),
      );
    }
  }
}
