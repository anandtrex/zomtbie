import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../api/collections_and_items.dart';
import '../models/collection.dart';
import '../models/provider/current_collection.dart';
import '../models/provider/current_group.dart';
import 'collection_item.dart';

class CollectionsScreen extends StatefulWidget {
  static const routeName = '/collections';
  final CurrentCollection currentCollection;
  final CurrentGroup currentGroup;
  final String searchQuery;

  const CollectionsScreen(
      {Key key,
      this.currentCollection,
      this.searchQuery = "",
      this.currentGroup})
      : super(key: key);

  @override
  _CollectionsState createState() => _CollectionsState();
}

class _CollectionsState extends State<CollectionsScreen> {
  static const _pageSize = 20;
  PagingController<int, Collection> _pagingController =
      PagingController(firstPageKey: 0, invisibleItemsThreshold: 10);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await fetchCollections(
          widget.currentCollection?.collection,
          widget.currentGroup?.group,
          pageKey,
          _pageSize,
          widget.searchQuery);
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    print("currentCollection: ${widget.currentCollection}");
    return PagedSliverList<int, Collection>(
      pagingController: _pagingController,
      shrinkWrapFirstPageIndicators: true,
      builderDelegate: PagedChildBuilderDelegate<Collection>(
        itemBuilder: (context, item, index) => _buildRow(item),
        noItemsFoundIndicatorBuilder: (_) => Center(
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Text('Collections Empty'),
          ),
        ),
      ),
    );
  }

  Widget _buildRow(Collection collection) {
    return ListTile(
      title: Text(
        collection.data_name,
        maxLines: 1,
        softWrap: true,
        overflow: TextOverflow.ellipsis,
        style: GoogleFonts.robotoSlab(),
      ),
      leading: Icon(
        Icons.folder,
        color: null,
      ),
      trailing: Icon(
        Icons.chevron_right,
        color: null,
      ),
      onTap: () {
        print("Updating collection to $collection");
        widget.currentCollection.push(collection);
        if (widget.searchQuery.length > 0) {
          Navigator.pop(context);
        }
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => WillPopScope(
              onWillPop: () async {
                widget.currentCollection.pop();
                return true;
              },
              child: CollectionItemScreen(
                widget.currentGroup,
                widget.currentCollection,
                key: Key(
                    '${widget.currentGroup.group?.data_name}${widget.currentCollection.collection?.data_name}'),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }
}
