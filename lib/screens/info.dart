import 'package:flutter/services.dart';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../models/item.dart';

class InfoScreen extends StatelessWidget {
  final Item currentZotItem;

  const InfoScreen(this.currentZotItem, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final blacklistedKeys = ['key', 'version', 'relations', 'collections'];
    return Container(
      padding: EdgeInsets.all(10),
      child: ListView(
        children: [
          ...currentZotItem.data.entries
              .where((entry) =>
                  !blacklistedKeys.contains(entry?.key) &&
                  entry.value.length > 0)
              .map((entry) {
            switch (entry?.key) {
              case 'creators':
                return Column(
                  children: [
                    ...currentZotItem.itemCreators.creators.map((e) {
                      return getFormField(context, e.item1, e.item2);
                    }),
                  ],
                );
              case 'tags':
                final tagList =
                    currentZotItem.itemTags.tags.map((e) => e.item1).join(", ");
                return getFormField(context, entry.key, tagList);
              default:
                return getFormField(context, entry.key, entry.value);
            }
          }).toList(),
        ],
      ),
    );
  }

  Widget getFormField(context, label, text) {
    return Stack(alignment: Alignment.centerRight, children: <Widget>[
      TextFormField(
        maxLines: null,
        decoration: InputDecoration(
          labelText: '$label',
          contentPadding: const EdgeInsets.fromLTRB(6, 6, 48, 6), // 48 -> icon width
        ),
        controller: TextEditingController(text: '$text'),
        enabled: false,
      ),
      IconButton(
        icon: const Icon(Icons.content_copy_rounded),
        tooltip: 'Copy to clipboard',
        onPressed: () {
          Clipboard.setData(new ClipboardData(text: text)).then((value) {
            final snackBar = SnackBar(
              content: Text('Copied to Clipboard'),
              // action: SnackBarAction(
              //   label: 'Undo',
              //   onPressed: () {},
              // ),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          });
        },
      ),
    ]);
  }
}
