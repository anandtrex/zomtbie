import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../models/item.dart';
import '../models/provider/current_group.dart';
import '../screens/attachments.dart';
import '../screens/info.dart';
import 'appbar.dart';

class ItemDetailsScreen extends StatelessWidget {
  final Item currentZotItem;
  final CurrentGroup currentGroup;

  const ItemDetailsScreen({Key key, this.currentZotItem, this.currentGroup})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appBar = ZomtbieAppBar.getAppBar(
      context,
      currentZotItem.data_title,
      tabbar: TabBar(tabs: [
        Tab(
          text: 'Info',
        ),
        Tab(
          text: 'Attachments',
        ),
      ]),
    );
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: appBar,
          body: TabBarView(children: [
            InfoScreen(currentZotItem),
            AttachmentsScreen(currentGroup, currentZotItem)
          ]),
        ));
  }
}
