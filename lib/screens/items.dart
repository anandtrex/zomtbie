import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

import '../api/collections_and_items.dart';
import '../models/item.dart';
import '../models/provider/current_collection.dart';
import '../models/provider/current_group.dart';
import 'item_details.dart';

class ItemsScreen extends StatefulWidget {
  static const routeName = '/items';
  final CurrentCollection currentCollection;
  final String searchQuery;
  final CurrentGroup currentGroup;

  const ItemsScreen(
      {Key key,
      this.currentCollection,
      this.searchQuery = "",
      this.currentGroup})
      : super(key: key);

  @override
  _ItemsState createState() => _ItemsState();
}

class _ItemsState extends State<ItemsScreen> {
  static const _pageSize = 20;
  final PagingController<int, Item> _pagingController =
      PagingController(firstPageKey: 0, invisibleItemsThreshold: 10);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      _fetchPage(pageKey);
    });
    super.initState();
  }

  Future<void> _fetchPage(int pageKey) async {
    try {
      final newItems = await fetchItems(
        widget.currentCollection?.collection,
        widget.currentGroup?.group,
        pageKey,
        _pageSize,
        widget.searchQuery,
      );
      final isLastPage = newItems.length < _pageSize;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      print("Error in parseing items: $error");
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return PagedSliverList<int, Item>(
      pagingController: _pagingController,
      shrinkWrapFirstPageIndicators: true,
      builderDelegate: PagedChildBuilderDelegate<Item>(
        itemBuilder: (context, item, index) => _buildRow(item),
        noItemsFoundIndicatorBuilder: (_) => Center(
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Text('Items Empty'),
          ),
        ),
      ),
    );
  }

  Widget _buildRow(Item item) {
    return ListTile(
      title: Text(
        item.data_title,
        maxLines: 2,
        softWrap: true,
        overflow: TextOverflow.ellipsis,
        style: GoogleFonts.robotoSlab(),
      ),
      leading: Icon(
        Icons.article_outlined,
        color: null,
      ),
      trailing: Icon(
        Icons.chevron_right,
        color: null,
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ItemDetailsScreen(
              currentGroup: widget.currentGroup,
              currentZotItem: item,
              key: Key(
                  '${widget.currentGroup.group?.data_name}${item?.data_title}'),
            ),
          ),
        );
      },
    );
  }
}
