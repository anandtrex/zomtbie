import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

import '../screens/start.dart';
import '../storage/shared_prefs.dart';
import 'appbar.dart';

class LoginScreen extends StatefulWidget {
  final String url;

  LoginScreen({Key key, @required this.url}) : super(key: key);

  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final flutterWebviewPlugin = new FlutterWebviewPlugin();

  StreamSubscription _onDestroy;
  StreamSubscription<String> _onUrlChanged;
  StreamSubscription<WebViewStateChanged> _onStateChanged;

  @override
  void dispose() {
    // Every listener should be canceled, the same should be done with this stream.
    _onDestroy.cancel();
    _onUrlChanged.cancel();
    _onStateChanged.cancel();
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    flutterWebviewPlugin.close();

    // Add a listener to on destroy WebView, so you can make came actions.
    _onDestroy = flutterWebviewPlugin.onDestroy.listen((_) {
      print("destroy");
    });

    _onStateChanged =
        flutterWebviewPlugin.onStateChanged.listen((WebViewStateChanged state) {
      print("onStateChanged: ${state.type} ${state.url}");
    });

    // Add a listener to on url changed
    _onUrlChanged = flutterWebviewPlugin.onUrlChanged.listen((String url) {
      if (mounted) {
        setState(() {
          print("URL changed: $url");
          if (url.startsWith(OAUTH_CALLBACK)) {
            final RegExp regExpToken = new RegExp("oauth_token=(.*)&");
            final oauth_token = regExpToken.firstMatch(url)?.group(1);
            print("oauth_token $oauth_token");

            final RegExp regExpVerifier = new RegExp("oauth_verifier=(.*)");
            final oauth_verifier = regExpVerifier.firstMatch(url)?.group(1);
            print("oauth_verifier $oauth_verifier");

            saveOAuthTokensToSharedPrefs(oauth_token, oauth_verifier)
                .then((value) {
              Navigator.of(context).pop();
              flutterWebviewPlugin.close();
            });
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new WebviewScaffold(
        url: widget.url,
        appBar: ZomtbieAppBar.getAppBar(context, "Login to Zotero...",
            showSearchIcon: false));
  }
}
