import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/provider/current_collection.dart';
import '../models/provider/current_group.dart';
import 'collection_item.dart';

class Search extends SearchDelegate {
  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () {
          Navigator.pop(context);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    final currentCollection = context.watch<CurrentCollection>();
    final currentGroup = context.watch<CurrentGroup>();
    return CollectionItemScreenNoScaffold(
      currentGroup,
      currentCollection,
      searchQuery: query,
      key: Key(
          '${currentGroup.group?.data_name}${currentCollection.collection?.data_name}'),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Container();
  }
}
