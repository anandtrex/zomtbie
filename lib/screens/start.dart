import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:oauth1/oauth1.dart' as oauth1;
import 'package:provider/provider.dart';

import '../consts.dart';
import '../models/provider/current_collection.dart';
import '../models/provider/current_group.dart';
import '../models/secrets.dart';
import '../models/shared_prefs.dart';
import '../storage/shared_prefs.dart';
import 'collection_item.dart';
import 'login.dart';

const String OAUTH_CALLBACK = "zomtbie://oauth_callback";

class StartScreen extends StatefulWidget {
  final PreOAuthSecrets secrets;

  const StartScreen({Key key, this.secrets}) : super(key: key);

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  final platform = new oauth1.Platform(
      'https://www.zotero.org/oauth/request', // temporary credentials request
      'https://www.zotero.org/oauth/authorize', // resource owner authorization
      'https://www.zotero.org/oauth/access', // token credentials request
      oauth1.SignatureMethods.hmacSha1 // signature method
      );

  bool credentialsAvailable = false;
  oauth1.ClientCredentials clientCredentials;
  oauth1.Credentials oAuthCredentials;
  oauth1.Authorization auth;

  @override
  void initState() {
    super.initState();
    clientCredentials = new oauth1.ClientCredentials(
        widget.secrets.zoteroApiKey, widget.secrets.zoteroApiSecret);
    auth = new oauth1.Authorization(clientCredentials, platform);
  }

  @override
  void didChangeDependencies() {
    _setCredentialState();
    getUserDetailsToSharedPrefs().then((UserDetails userDetails) {
      // print("userDetails: ${userDetails?.username}, ${userDetails?.userID}");
    });
  }

  void _setCredentialState() async {
    oauth1.Credentials tokenCredentialsOAuth = await getCredentials(auth);
    if (tokenCredentialsOAuth != null) {
      setState(() {
        credentialsAvailable = true;
        oAuthCredentials = tokenCredentialsOAuth;
      });
    } else {
      setState(() {
        credentialsAvailable = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    print("credentialsAvailable: $credentialsAvailable");
    final currentCollection = context.watch<CurrentCollection>();
    final currentGroup = context.watch<CurrentGroup>();

    if (credentialsAvailable) {
      currentGroup.update(null);
      currentCollection.clear();
      return CollectionItemScreen(currentGroup, currentCollection,
          key: Key(
              '${currentGroup.group?.data_name}${currentCollection.collection?.data_name}'));
    }
    return Scaffold(
        body: Column(
          children: [
            Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 50),
                  child: Image.asset(
                    "assets/papers.jpg",
                    fit: BoxFit.fitWidth,
                  ),
                ),
                Image.asset(
                  "zomtbie-circle-icon.png",
                  width: 200,
                  height: 200,
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: Text(
                ' Welcome to Zomtbie \n for Zotero ',
                textAlign: TextAlign.center,
                style: GoogleFonts.raleway(
                  textStyle: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    backgroundColor: Color.fromARGB(100, 255, 255, 255),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: Text(
                ' Please Log in... ',
                textAlign: TextAlign.center,
                style: GoogleFonts.raleway(
                  textStyle: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    backgroundColor: Color.fromARGB(100, 255, 255, 255),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
              child: _buildButton(context),
            ),

          ],
        ));
  }

  Widget _buildButton(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 50,
        width: 150,
        child: ElevatedButton(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(THEME_COLOR),
          ),
          onPressed: _handleLoginPress,
          child: Text(
            "Login to Zotero",
            style: TextStyle(fontSize: 16, color: Colors.black),
          ),
        ),
      ),
    );
  }

  void _handleLoginPress() async {
    // request temporary credentials (request tokens)
    oauth1.AuthorizationResponse res =
        await auth.requestTemporaryCredentials(OAUTH_CALLBACK);
    saveTokenCredentialsToSharedPrefs("temporary", res.credentials);
    final suffixString =
        '&library_access=1&notes_access=1&write_access=1&all_groups=write';
    await Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => LoginScreen(
              url:
                  auth.getResourceOwnerAuthorizationURI(res.credentials.token) +
                      suffixString)),
    );
    _setCredentialState();
  }

  static Future<oauth1.Credentials> getCredentials(auth) async {
    final tokenCredentialsTemp =
        await getTokenCredentialsFromSharedPrefs("temporary");
    if (tokenCredentialsTemp != null) {
      final oauthState = await getOAuthTokensFromSharedPrefs();
      if (oauthState != null) {
        // print("tokenCredentials and value: $tokenCredentialsTemp, $oauthState");
        final tokenCredentialsOAuth =
            await getTokenCredentialsFromSharedPrefs("oauth");
        if (tokenCredentialsOAuth != null) {
          return tokenCredentialsOAuth;
        } else {
          // request token credentials (access tokens)
          final res = await auth.requestTokenCredentials(
              tokenCredentialsTemp, oauthState.verifier);
          // print("Got token credentials");
          // print("Optional parameters are: ${res?.optionalParameters}");
          saveUserDetailsToSharedPrefs(res?.optionalParameters['userID'],
              res?.optionalParameters['username']);
          await saveTokenCredentialsToSharedPrefs(
              FINAL_TOKEN_CREDENTIALS_PREFIX, res.credentials);
          return oauth1.Credentials(
              res.credentials.token, res.credentials.tokenSecret);
        }
      } else {
        // print("oauthState is ${oauthState}");
      }
    } else {
      // print("tokenCredentialsTemp is ${tokenCredentialsTemp}");
    }
  }
}
