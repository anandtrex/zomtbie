import 'package:oauth1/oauth1.dart' as oauth1;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/shared_prefs.dart';

const String USER_ID = "userID";
const String USER_NAME = "username";
const String TOKEN_CREDENTIALS = "token_credentials";
const String TOKEN_CREDENTIALS_SECRET = "token_credentials_secret";
const String OAUTH_TOKEN = "oauth_token";
const String OAUTH_VERIFIER = "oauth_verifier";

void clearPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.clear();
  prefs.reload();
}

void saveUserDetailsToSharedPrefs(String userID, String username) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString(USER_ID, userID);
  await prefs.setString(USER_NAME, username);
}

Future<UserDetails> getUserDetailsToSharedPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final userID = await prefs.getString(USER_ID);
  final username = await prefs.getString(USER_NAME);
  if (userID != null && username != null) {
    return UserDetails(userID: userID, username: username);
  }
}

void saveTokenCredentialsToSharedPrefs(
    String prefix, oauth1.Credentials credentials) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  // print(
  //     "Token credentials ($prefix) are: ${credentials.token}, ${credentials.tokenSecret}");
  await prefs.setString('${prefix}_${TOKEN_CREDENTIALS}', credentials.token);
  await prefs.setString(
      '${prefix}_${TOKEN_CREDENTIALS_SECRET}', credentials.tokenSecret);
}

Future<oauth1.Credentials> getTokenCredentialsFromSharedPrefs(
    String prefix) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final token = await prefs.getString('${prefix}_${TOKEN_CREDENTIALS}');
  final tokenSecret =
      await prefs.getString('${prefix}_${TOKEN_CREDENTIALS_SECRET}');
  if (token != null && tokenSecret != null) {
    // print("Returning token and tokenSecret: $token, $tokenSecret");
    return oauth1.Credentials(token, tokenSecret);
  }
}

Future<void> saveOAuthTokensToSharedPrefs(token, verifier) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setString(OAUTH_TOKEN, token);
  await prefs.setString(OAUTH_VERIFIER, verifier);
}

Future<OAuthState> getOAuthTokensFromSharedPrefs() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  final token = await prefs.getString(OAUTH_TOKEN);
  final verifier = await prefs.getString(OAUTH_VERIFIER);
  if (token != null && verifier != null) {
    // print("Returning (OAuth) token and verifier: $token, $verifier");
    return OAuthState(token: token, verifier: verifier);
  }
}
